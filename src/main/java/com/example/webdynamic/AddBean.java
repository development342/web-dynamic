package com.example.webdynamic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.ejb.Stateless;

@NoArgsConstructor
@Getter
@Setter
@Stateless
public class AddBean {

    private int firstNumber, secondNumber, resultNumber;

    public int add() {
        resultNumber = firstNumber + secondNumber;
        return resultNumber;
    }
}
