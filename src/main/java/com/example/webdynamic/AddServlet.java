package com.example.webdynamic;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = {"/AddServlet"})
public class AddServlet extends HttpServlet {

    @EJB
    AddBean addBean;

    public void doPost(HttpServletRequest servletRequest, HttpServletResponse httpServletResponse) throws IOException {
        PrintWriter printWriter = httpServletResponse.getWriter();
        int firstNumber = Integer.parseInt(servletRequest.getParameter("firstNumber"));
        int secondNumber = Integer.parseInt(servletRequest.getParameter("secondNumber"));

        addBean.setFirstNumber(firstNumber);
        addBean.setSecondNumber(secondNumber);

        int resultNumber = addBean.add();

        printWriter.println("Addition is : " + resultNumber);
    }
}
